﻿namespace Calculator
{
    public interface ICalculator
    {
        double Calculate(string expression);
    }
}
