﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class BracesNotBalancedException : ApplicationException { }

    public class IncorrectExpressionException : ApplicationException { }

    public class WrongOperandTypeException : ApplicationException { }

    public enum LexemaType
    {
        Operation, Operand, OpenBrace, CloseBrace, StartStop
    }

    // Struct to store parsed "lexemas"
    public struct Lexema
    {
        public string Value { get; set; }
        public LexemaType LexemaType { get; set; }
    }

    public interface ILexemaParser
    {
        IEnumerable<AbstractOperation> AcceptedOperations { get; }
        IEnumerable<Lexema> GetLexemas(string expression);
    }

    public class LexemaParser : ILexemaParser
    {
        // Accepted operations
        private readonly IOperationManager _operationManager;

        // buffer for symbols
        private string _buffer = "";

        // check if current symbol is an operation
        private bool _isOperation(string c)
        {
            return _operationManager.HasOperation(c);
        }

        // check if current symbol is a digit 
        private bool _isDigit(char c)
        {
            byte b;
            return byte.TryParse(c.ToString(), out b);
        }

        // check if current symbol is a decimal separator
        private bool _isDecimalSeparator(char c)
        {
            return c == DecimalSeparator;
        }

        // check if current symbol is an open brace
        private bool _isOpenBrace(char c)
        {
            return c == '(';
        }

        // check if current symbol is a close brace
        private bool _isCloseBrace(char c)
        {
            return c == ')';
        }

        private void _flushBuffer()
        {
            if (!string.IsNullOrEmpty(_buffer))
            {
                _lexemas.Add(new Lexema { LexemaType = LexemaType.Operand, Value = _buffer });
                _buffer = "";
            }
        }

        // check if open and close braces are match
        private bool _areBracesBalanced()
        {
            var countOpenBraces = Expression.Count(t => t == '(');
            var countCloseBraces = Expression.Count(t => t == ')');
            return countOpenBraces == countCloseBraces;
        }

        private readonly List<Lexema> _lexemas = new List<Lexema>();

        // get all lexemas from expression as IEnumerable
        private void _getLexemas()
        {
            var canBeUnary = true;
            foreach (var symbol in Expression)
            {
                if (symbol == ' ')
                {
                    _flushBuffer();
                    continue;
                }

                if (_isOpenBrace(symbol))
                {
                    _flushBuffer();
                    _lexemas.Add(new Lexema { LexemaType = LexemaType.OpenBrace, Value = symbol.ToString() });
                    canBeUnary = true;
                }
                else if (_isCloseBrace(symbol))
                {
                    _flushBuffer();
                    _lexemas.Add(new Lexema { LexemaType = LexemaType.CloseBrace, Value = symbol.ToString() });
                    canBeUnary = false;
                }
                else if (_isOperation(symbol.ToString()))
                {
                    if (canBeUnary)
                    {
                        _buffer += symbol;
                        canBeUnary = false;
                    }
                    else
                    {
                        _flushBuffer();
                        _lexemas.Add(new Lexema { LexemaType = LexemaType.Operation, Value = symbol.ToString() });
                    }
                }
                else if (_isDigit(symbol) || _isDecimalSeparator(symbol))
                {
                    _buffer += symbol;
                    canBeUnary = false;
                }
                else
                {
                    throw new IncorrectExpressionException();
                }
            }
            _flushBuffer();
        }

        protected string Expression = "";

        public char DecimalSeparator { get; set; }

        public LexemaParser(IOperationManager operationManager)
        {
            _operationManager = operationManager;
            DecimalSeparator = '.';
        }

        public IEnumerable<AbstractOperation> AcceptedOperations { get { return _operationManager.Operations; } }

        public IEnumerable<Lexema> GetLexemas(string expression)
        {
            Expression = expression;
            if (!_areBracesBalanced())
            {
                throw new BracesNotBalancedException();
            }
            _lexemas.Clear();
            _getLexemas();
            return _lexemas;
        }
    }
}
