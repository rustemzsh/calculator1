﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            IOperationManager operationManager = new OperationManager();
            operationManager.AddOperation(new PlusOperation());
            operationManager.AddOperation(new MinusOperation());
            operationManager.AddOperation(new MultiplyOperation());
            operationManager.AddOperation(new DivideOperation());
            ILexemaParser lexemaParser = new LexemaParser(operationManager);
            IPolishConverter converter = new PolishConverter(operationManager);

            ICalculator calc = new PolishCalculator(lexemaParser, converter, operationManager);

            Console.WriteLine("Welcome to Simple Console Calculator 1.0a");
            Console.WriteLine("Please enter expression, e.g. 5+2");

            Console.Write("> ");
            string input;
            while ((input = Console.ReadLine()) != "q")
            {
                if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(input.Trim()))
                {
                    Console.WriteLine("Input string is empty");
                    Console.Write("> ");
                    continue;
                }
                try
                {
                    var result = calc.Calculate(input);
                    Console.WriteLine("> {0}", result);
                }
                catch (IncorrectExpressionException e)
                {
                    Console.WriteLine("Your expression is incorrect. Please try again..");
                }
                catch (WrongOperandTypeException e)
                {
                    Console.WriteLine("Operand is not accepted");
                }
                catch (BracesNotBalancedException e)
                {
                    Console.WriteLine("Braces are not balanced");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ooops, something wrong with your expression... {0}", e.Message);
                }
                Console.Write("> ");
            }

            Console.WriteLine("Good luck!");
        }
    }
}
