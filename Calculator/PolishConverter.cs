﻿using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public interface IPolishConverter
    {
        Queue<Lexema> GetPolishNotation(IEnumerable<Lexema> lexemas);
    }

    // Converter from List of Lexemas to reverse Polish notation
    public class PolishConverter: IPolishConverter
    {
        private readonly IOperationManager _operationManager;

        public PolishConverter(IOperationManager operationManager)
        {
            _operationManager = operationManager;
        }

        // buffer for temporary operations
        private readonly Stack<Lexema> _buffStack = new Stack<Lexema>();

        // final "polish" queue ready for calcualtions
        private readonly Queue<Lexema> _finalQueue = new Queue<Lexema>();

        // contains current position when parsing expression
        private short _currentPos;

        private void _op1(Lexema lexema)
        {
            _buffStack.Push(lexema);
            _currentPos++;
        }

        private void _op2()
        {
            var lex = _buffStack.Pop();
            _finalQueue.Enqueue(lex);
        }

        private void _op3()
        {
            _buffStack.Pop();
            _currentPos++;
        }

        private readonly List<Lexema> _lexemas = new List<Lexema>();

        // return Operation by Symbol
        private AbstractOperation _getOperation(Lexema lexema)
        {
            return _operationManager.GetOperation(lexema.Value);
        }

        // create queue with reverse polish notation from list of lexemas
        public Queue<Lexema> GetPolishNotation(IEnumerable<Lexema> lexemas)
        {
            _lexemas.Clear();
            _lexemas.AddRange(lexemas);
            _currentPos = 0;
            _buffStack.Clear();
            _finalQueue.Clear();
            _lexemas.Add(new Lexema { LexemaType = LexemaType.StartStop });
            _buffStack.Push(new Lexema { LexemaType = LexemaType.StartStop });
            var end = false;
            while (!end)
            {
                var lexema = _lexemas[_currentPos];
                var prev = _buffStack.Peek();
                switch (lexema.LexemaType)
                {
                    case LexemaType.Operand:
                        _finalQueue.Enqueue(lexema);
                        _currentPos++;
                        break;
                    case LexemaType.StartStop:
                        if (prev.LexemaType == LexemaType.StartStop)
                        {
                            end = true;
                        }
                        else if (prev.LexemaType == LexemaType.OpenBrace)
                        {
                            throw new IncorrectExpressionException();
                        }
                        else
                        {
                            _op2();
                        }
                        break;
                    case LexemaType.Operation:
                        if (prev.LexemaType == LexemaType.StartStop || prev.LexemaType == LexemaType.OpenBrace)
                        {
                            _op1(lexema);
                        }
                        else if (prev.LexemaType == LexemaType.CloseBrace)
                        {
                            throw new IncorrectExpressionException();
                        }
                        else
                        {
                            var op = _getOperation(lexema);

                            if (op.Priority == Priority.Low)
                            {
                                if (prev.LexemaType == LexemaType.StartStop || prev.LexemaType == LexemaType.OpenBrace)
                                {
                                    _op1(lexema);
                                }
                                else
                                {
                                    _op2();
                                }
                            } 
                            else if (op.Priority == Priority.High)
                            {
                                if (prev.LexemaType == LexemaType.Operation)
                                {
                                    // operation in buffer stack
                                    var pop = _getOperation(_buffStack.Peek());
                                    if (pop.Priority == Priority.High)
                                    {
                                        _op2();
                                    }
                                    else
                                    {
                                        _op1(lexema);
                                    }
                                }
                            }
                        }
                        break;
                    case LexemaType.OpenBrace:
                        _op1(lexema);
                        break;
                    case LexemaType.CloseBrace:
                        if (prev.LexemaType == LexemaType.StartStop)
                        {
                            throw new IncorrectExpressionException();
                        }
                        if (prev.LexemaType == LexemaType.OpenBrace)
                        {
                            _op3();
                        }
                        if (prev.LexemaType == LexemaType.Operation || prev.LexemaType == LexemaType.Operand ||
                            prev.LexemaType == LexemaType.Operation)
                        {
                            _op2();
                        }
                        break;
                }
            }
            return _finalQueue;
        }
    }
}
