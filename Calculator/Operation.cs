﻿using System;

namespace Calculator
{
    // Operation Priority
    public enum Priority { Low = 2, High = 3 }

    // Abstract base Operation class
    public abstract class AbstractOperation
    {
        public abstract byte OperandsCount { get; }
        public abstract Priority Priority { get; }
        public abstract string Symbol { get; }
        public abstract double Execute(double[] operands);
    }

    public abstract class BinaryOperation : AbstractOperation
    {
        public override byte OperandsCount
        {
            get { return 2; }
        }
    }

    public sealed class PlusOperation : BinaryOperation
    {
        public override Priority Priority
        {
            get { return Priority.Low; }
        }

        public override string Symbol
        {
            get { return "+"; }
        }

        public override double Execute(double[] operands)
        {
            return operands[0] + operands[1];
        }
    }

    public sealed class MinusOperation : BinaryOperation
    {
        public override Priority Priority
        {
            get { return Priority.Low; }
        }

        public override string Symbol
        {
            get { return "-"; }
        }

        public override double Execute(double[] operands)
        {
            return operands[0] - operands[1];
        }
    }

    public sealed class MultiplyOperation : BinaryOperation
    {
        public override Priority Priority
        {
            get { return Priority.High; }
        }

        public override string Symbol
        {
            get { return "*"; }
        }

        public override double Execute(double[] operands)
        {
            return operands[0] * operands[1];
        }
    }

    public sealed class DivideOperation : BinaryOperation
    {
        public override Priority Priority
        {
            get { return Priority.High; }
        }

        public override string Symbol
        {
            get { return "/"; }
        }

        public override double Execute(double [] operands)
        {
            return operands[0]/operands[1];
        }
    }

    public sealed class PowerOperation : BinaryOperation
    {
        public override Priority Priority
        {
            get { return Priority.High; }
        }

        public override string Symbol
        {
            get { return "^"; }
        }

        public override double Execute(double[] operands)
        {
            return Math.Pow(operands[0], operands[1]);
        }
    }
}
