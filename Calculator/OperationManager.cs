﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public interface IOperationManager
    {
        IEnumerable<AbstractOperation> Operations { get; }
        void AddOperation(AbstractOperation operation);
        AbstractOperation GetOperation(string symbol);
        bool HasOperation(string symbol);
    }

    public class OperationManager : IOperationManager
    {
        private readonly List<AbstractOperation> _operations;

        public OperationManager()
        {
            _operations = new List<AbstractOperation>();
        }

        public void AddOperation(AbstractOperation operation)
        {
            if (_operations.All(s => s.Symbol != operation.Symbol))
            {
                _operations.Add(operation);
            }
        }

        public IEnumerable<AbstractOperation> Operations
        {
            get { return _operations; }
        }

        public AbstractOperation GetOperation(string symbol)
        {
            var operation = _operations.FirstOrDefault(s => s.Symbol == symbol);
            if (operation == null)
            {
                throw new InvalidOperationException("Opration not found");
            }
            return operation;
        }

        public bool HasOperation(string symbol)
        {
            return _operations.Any(s => s.Symbol == symbol);
        }
    }
}
