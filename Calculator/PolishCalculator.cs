﻿using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public abstract class AbstractPolishCalculator : ICalculator
    {
        protected ILexemaParser LexemaParser;
        protected IOperationManager OperationManager;

        protected AbstractPolishCalculator(
            ILexemaParser lexemaParser, 
            IOperationManager operationManager)
        {
            LexemaParser = lexemaParser;
            OperationManager = operationManager;
        }

        public abstract double Calculate(string expression);
    }

    // Calculator uses reverse Polish notation for calculations
    public class PolishCalculator : AbstractPolishCalculator
    {
        // execute operation 
        private double _executeOperation(AbstractOperation operation, Stack<double> stack)
        {
            List<double> operands = new List<double>(operation.OperandsCount);
            for (int i = 0; i < operation.OperandsCount; i++)
            {
                operands.Insert(0, stack.Pop());
            }
            return operation.Execute(operands.ToArray());
        }

        private double _calculate()
        {
            var stack = new Stack<double>();
            foreach (var lexema in _finalQueue)
            {
                double operand;
                if (lexema.LexemaType == LexemaType.Operand)
                {
                    var op = lexema.Value.Replace('.', ',');
                    if (!double.TryParse(op, out operand))
                    {
                        throw new WrongOperandTypeException();
                    }
                    stack.Push(operand);
                }
                else
                {
                    var operation = OperationManager.GetOperation(lexema.Value);
                    var res = _executeOperation(operation, stack);
                    stack.Push(res);
                }
            }
            return stack.Pop();
        }

        private List<Lexema> _lexemas;
        private Queue<Lexema> _finalQueue;

        private readonly IPolishConverter _converter;

        protected string Expression = "";

        public PolishCalculator(
            ILexemaParser lexemaParser, 
            IPolishConverter converter,
            IOperationManager operationManager)
            : base(lexemaParser, operationManager)
        {
            _converter = converter;
        }

        public override double Calculate(string expression)
        {
            Expression = expression;
            
            _lexemas = LexemaParser.GetLexemas(Expression).ToList();

            _finalQueue = _converter.GetPolishNotation(_lexemas);

            return _calculate();
        }

    }
}
