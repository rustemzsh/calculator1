﻿using System;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// Test class for Operations
namespace CalculatorTest
{
    [TestClass]
    public class OperationTest
    {
        [TestMethod]
        public void TestPlusOperation()
        {
            var operation = new PlusOperation();

            var res = operation.Execute(new double[] {5, 4});

            Assert.AreEqual(res, 9, 0.0001);
        }
    }
}
