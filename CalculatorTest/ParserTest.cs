﻿using System.Linq;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    [TestClass]
    public class ParserTest
    {
        private readonly IOperationManager _operationManager = new OperationManager();

        private readonly LexemaParser _lexemaParser;

        public ParserTest()
        {
            _operationManager.AddOperation(new PlusOperation());
            _operationManager.AddOperation(new MinusOperation());
            _operationManager.AddOperation(new MultiplyOperation());
            _operationManager.AddOperation(new DivideOperation());
            _lexemaParser = new LexemaParser(_operationManager);
        }

        [TestMethod]
        public void TestParseInts()
        {
            // arrange
            var expression = "5+5";

            // act
            var lexemas = _lexemaParser.GetLexemas(expression);
            var enumerable = lexemas as Lexema[] ?? lexemas.ToArray();
            var res = enumerable.Aggregate("", (current, lexema) => current + (lexema.Value + " "));

            // assert
            Assert.AreEqual("5 + 5 ", res);
        }

        [TestMethod]
        public void TestParseDouble()
        {
            // arrange
            var expression = "5.5+5.2";

            // act
            var lexemas = _lexemaParser.GetLexemas(expression);
            var enumerable = lexemas as Lexema[] ?? lexemas.ToArray();
            var res = enumerable.Aggregate("", (current, lexema) => current + (lexema.Value + " "));

            // assert
            Assert.AreEqual("5.5 + 5.2 ", res);
        }

        [TestMethod]
        public void TestComplexExpression()
        {
            // arrange
            var expression = "(5.5+5.2) - (-4)*4 + 2";

            // act
            var lexemas = _lexemaParser.GetLexemas(expression);
            var enumerable = lexemas as Lexema[] ?? lexemas.ToArray();
            var res = enumerable.Aggregate("", (current, lexema) => current + (lexema.Value + " "));

            // assert
            Assert.AreEqual("( 5.5 + 5.2 ) - ( -4 ) * 4 + 2 ", res);
        }

        [TestMethod]
        public void TestDecimalSeparator()
        {
            // arrange
            var parser = new LexemaParser(_operationManager);
            parser.DecimalSeparator = ',';
            var expression = "(5,5+5,2) - (-4)*4 + 2";

            // act
            var lexemas = parser.GetLexemas(expression);
            var enumerable = lexemas as Lexema[] ?? lexemas.ToArray();
            var res = enumerable.Aggregate("", (current, lexema) => current + (lexema.Value + " "));

            // assert
            Assert.AreEqual("( 5,5 + 5,2 ) - ( -4 ) * 4 + 2 ", res);
        }

        [TestMethod]
        [ExpectedException(typeof(BracesNotBalancedException))]
        public void TestBracesNotBalanced()
        {
            // arrange
            const string expression = "(38 + (333 - (5*3))";

            // act
            _lexemaParser.GetLexemas(expression);
        }

        [TestMethod]
        [ExpectedException(typeof(IncorrectExpressionException))]
        public void TestWrongOperand()
        {
            // arrange
            const string expression = "(82 - 7f + 4)";

            // act
            _lexemaParser.GetLexemas(expression);
        }
    }
}
