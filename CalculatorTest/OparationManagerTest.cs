﻿using System;
using System.Linq;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    [TestClass]
    public class OparationManagerTest
    {
        [TestMethod]
        public void TestAddOperation()
        {
            var operationManager = new OperationManager();
            
            operationManager.AddOperation(new PlusOperation());
            operationManager.AddOperation(new MinusOperation());

            Assert.AreEqual(2, operationManager.Operations.Count());
        }

        [TestMethod]
        public void TestAddNotUniqueOperationAdd()
        {
            var operationManager = new OperationManager();
            operationManager.AddOperation(new PlusOperation());
            operationManager.AddOperation(new PlusOperation());

            Assert.AreEqual(1, operationManager.Operations.Count());
        }

        [TestMethod]
        public void TestHasOperation()
        {
            var operationManager = new OperationManager();
            operationManager.AddOperation(new PlusOperation());
            operationManager.AddOperation(new PlusOperation());

            Assert.AreEqual(true, operationManager.HasOperation("+"));
        }

        [TestMethod]
        public void TestGetOperation()
        {
            var operationManager = new OperationManager();
            var plusOperation = new PlusOperation();
            operationManager.AddOperation(plusOperation);
            operationManager.AddOperation(new PlusOperation());

            Assert.AreSame(plusOperation, operationManager.GetOperation("+"));
        }
    }
}
