﻿using System.Collections.Generic;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    [TestClass]
    public class CalculatorTests
    {
        private readonly OperationManager _operationManager = 
            new OperationManager();

        private readonly PolishCalculator _calc;

        public CalculatorTests()
        {
            _operationManager.AddOperation(new PlusOperation());
            _operationManager.AddOperation(new MinusOperation());
            _operationManager.AddOperation(new MultiplyOperation());
            _operationManager.AddOperation(new DivideOperation());
            var parser = new LexemaParser(_operationManager);
            var converter = new PolishConverter(_operationManager);
            _calc = new PolishCalculator(parser, converter, _operationManager);
        }

        [TestMethod]
        public void TestCalcExpression()
        {
            // arrange
            const string expression = "( 8 +2* 5)/(1 + 3*2 -(+4))";
            
            // act
            var result = _calc.Calculate(expression);

            // assert
            //var stringResult = string.Join(" ", result);
            Assert.AreEqual(6, result, 0.00001);
        }

    }
}
